"use strict";
let canvas = document.querySelector('canvas');
canvas.width = screen.availWidth;
canvas.height = screen.availHeight;
let ctx = canvas.getContext('2d');
let str = '01010asdas1010sads1011562212301'.split('');
let arr = new Array(Math.ceil(canvas.width / 10)).fill(0);
function render() {
    ctx.fillStyle = 'rgba(0, 0, 0,0.05)';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = '#0f0';
    arr.forEach((v, i) => {
        ctx.fillText(str[Math.floor(Math.random() * str.length)], i * 10, v + 10);
        arr[i] = arr[i] > canvas.height || arr[i] > Math.random() * 10000 ? 0 : arr[i] + 10;
    });
}
setInterval(render, 1000 / 60);
