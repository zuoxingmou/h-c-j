class createRectangle {
  // 面积对象列表的索引
  #id;
  // 创建的矩形的宽度
  #width;
  // 创建的矩形的高度
  #height;
  // 创建的矩形对象
  #rectangle;
  // 所有已创建的矩形对象列表
  #rectangles;
  // 矩形数量
  #num;

  /**
   * 创建一个createRectangle对象
   */
  constructor() {
    let that = this;

    // 存储事件
    addEventListener('storage', function (e) {
      // 监听storage事件，当存储发生变化时执行以下函数

      if (e.key == "rectangles") {
        // 如果存储事件的键为"rectangles"

        let didRectangleLengthChange = false;
        // 定义一个变量didRectangleLengthChange，并赋值为false

        if (that.#rectangles.length != JSON.parse(e.newValue).length)
          didRectangleLengthChange = true;
        // 如果that.#rectangles的长度不等于JSON.parse(e.newValue)的长度，则将didRectangleLengthChange赋值为true

        that.#rectangles = JSON.parse(e.newValue);
        // 将that.#rectangles赋值为JSON.parse(e.newValue)

        if (didRectangleLengthChange) {
          // 如果didRectangleLengthChange为true

          for (let i = 0; i < that.#rectangles.length; i++) {
            // 遍历that.#rectangles数组

            that.#rectangles[i].width = i * 20 + 150;
            // 将that.#rectangles[i]的width属性赋值为i * 20 + 150

            that.#rectangles[i].height = i * 20 + 200;
            // 将that.#rectangles[i]的height属性赋值为i * 20 + 200

            if (that.#rectangles[i].id == that.#id) {
              // 如果that.#rectangles[i]的id属性等于that.#id

              that.#width = that.#rectangles[i].width;
              // 将that.#width赋值为that.#rectangles[i]的width属性

              that.#height = that.#rectangles[i].height;
              // 将that.#height赋值为that.#rectangles[i]的height属性
            }
          }
        }
      }

      if (e.key == 'num') {
        // 如果存储事件的键为'num'

        that.#num = JSON.parse(e.newValue);
        // 将that.#num赋值为JSON.parse(e.newValue)
      }
    })

    // 退出前触发的事件
    window.addEventListener('beforeunload', function () {
      let index = that.getIndexById(that.#id);  // 获取指定id对应的索引
      that.#rectangles.splice(index, 1);  // 从rectangles数组中删除该索引位置的元素
      that.#num--;  // 数量减1
      that.saveRectangles(that.#rectangles);  // 保存修改后的rectangles数组
      that.saveNum(that.#num);  // 保存修改后的num值
    })
  }

  /**
   * 初始化方法
   */
  init() {
    // 从本地存储中获取矩形对象列表
    this.#rectangles = JSON.parse(localStorage.getItem("rectangles")) || [];
    // 获取矩形数量
    this.#num = JSON.parse(localStorage.getItem("num")) + 1 || 0 + 1;
    try {
      // 获取下一个矩形的id
      this.#id = this.#rectangles[this.#rectangles.length - 1].id + 1;
    } catch (e) {
      // 如果发生错误，则id为当前矩形数量
      this.#id = this.#num;
    }
    try {
      // 根据最后一个矩形的宽度和高度计算下一个矩形的宽度和高度
      this.#width = this.#rectangles[this.#rectangles.length - 1].width + 20;
      this.#height = this.#rectangles[this.#rectangles.length - 1].height + 20;
    } catch (e) {
      // 如果发生错误，则使用默认的宽度和高度
      this.#width = 150;
      this.#height = 200;
    }
    // 创建新的矩形对象
    this.#rectangle = {
      id: this.#id,
      width: this.#width,
      height: this.#height,
      space: this.getSpace()
    }
    // 将新的矩形对象添加到列表中
    this.#rectangles.push(this.#rectangle);
    // 将更新后的矩形对象列表保存到本地存储中
    this.saveRectangles(this.#rectangles);
    // 将更新后的矩形数量保存到本地存储中
    this.saveNum(this.#num);
  }

  /**
   * 获取屏幕空间
   * @returns 屏幕空间对象
   */
  getSpace() {
    return { max_x: window.innerWidth, max_y: window.innerHeight, x: window.screenLeft, y: window.screenTop }
  }

  /**
   * 保存矩形对象列表到本地存储中
   * @param rectangles 矩形对象列表
   */
  saveRectangles(rectangles) {
    localStorage.setItem("rectangles", JSON.stringify(rectangles));
  }

  /**
   * 保存矩形数量到本地存储中
   * @param num 矩形数量
   */
  saveNum(num) {
    localStorage.setItem("num", JSON.stringify(num));
  }

  /**
   * 根据id获取矩形对象列表中的索引
   * @param id 矩形id
   * @returns 索引
   */
  getIndexById(id) {
    let rectangles = JSON.parse(localStorage.getItem("rectangles"));
    for (let i = 0; i < rectangles.length; i++) {
      if (rectangles[i].id == id) {
        return i;
      }
    }
  }

  /**
   * 获取创建的矩形的宽度
   * @returns 矩形宽度
   */
  getRectangleWidth() {
    return this.#width;
  }

  /**
   * 获取创建的矩形的高度
   * @returns 矩形高度
   */
  getRectangleHeight() {
    return this.#height;
  }

  /**
   * 获取创建的矩形的id
   * @returns 矩形id
   */
  getRectangleId() {
    return this.#id;
  }

  /**
   * 获取所有已创建的矩形对象列表
   * @returns 矩形对象列表
   */
  getRectangles() {
    return this.#rectangles;
  }

  /**
   * 获取创建的矩形对象
   * @returns 创建的矩形对象
   */
  getRectangle() {
    return this.#rectangle;
  }

  /**
   * 获取创建的矩形的id
   * @returns 创建的矩形的id
   */
  getId() {
    return this.#id;
  }

  /**
   * 更新矩形对象的宽度、高度和位置信息
   */
  updated() {
    let space = this.getSpace();
    if (this.#rectangle.space.max_x != space.max_x || this.#rectangle.space.max_y != space.max_y
      || this.#rectangle.space.x != space.x || this.#rectangle.space.y != space.y) {
      this.#rectangle.space = space;
    }
    this.#rectangle.width = this.#width;
    this.#rectangle.height = this.#height;
    this.#rectangles[this.getIndexById(this.#id)] = this.#rectangle;
    this.saveRectangles(this.#rectangles);
  }

}

// 导出createRectangle类
export default createRectangle;