import createRectangle from "./createRectangle.js";
// 导入createRectangle函数

document.addEventListener('DOMContentLoaded', function () {
  // 页面加载完成后执行函数

  let deepCanvas = null;
  // 定义变量deepCanvas，并初始化为null

  function init() {
    // 定义函数init

    let rect = new createRectangle();
    // 创建createRectangle实例，并赋值给变量rect

    deepCanvas = createCanvasDeep();
    // 调用createCanvasDeep函数，并将返回值赋值给deepCanvas变量

    rect.init();
    // 调用rect对象的init方法

    createCanvas();
    // 调用createCanvas函数，创建canvas并添加resize事件监听实时更新宽高

    draw(rect);
    // 调用draw函数，绘制图形
  }
  init();

  function createCanvasDeep() {
    // 定义函数createCanvasDeep

    let canvas = document.createElement('canvas');
    // 创建canvas元素，并赋值给变量canvas

    const screenWidth = window.screen.availWidth;
    // 获取屏幕可用宽度，并赋值给变量screenWidth

    const screenHeight = window.screen.availHeight;
    // 获取屏幕可用高度，并赋值给变量screenHeight

    const zoomLevel = window.devicePixelRatio;
    // 获取设备像素比，并赋值给变量zoomLevel

    canvas.width = (screenWidth / zoomLevel.toFixed(2)).toFixed(2);
    // 计算canvas宽度，并赋值给canvas.width属性

    canvas.height = (screenHeight / zoomLevel.toFixed(2)).toFixed(2);
    // 计算canvas高度，并赋值给canvas.height属性

    canvas.style.backgroundColor = '#000';
    // 设置canvas的背景颜色为黑色

    canvas.style.width = (screenWidth / zoomLevel.toFixed(2)).toFixed(2) + 'px';
    // 计算canvas显示宽度，并赋值给canvas.style.width属性

    canvas.style.height = (screenHeight / zoomLevel.toFixed(2)).toFixed(2) + 'px';
    // 计算canvas显示高度，并赋值给canvas.style.height属性

    return canvas;
    // 返回canvas元素
  }
  function createCanvas() {
    // 定义函数createCanvas

    let canvas = document.createElement('canvas');
    // 创建canvas元素，并赋值给变量canvas

    canvas.width = window.innerWidth;
    // 设置canvas宽度为窗口内部宽度

    canvas.height = window.innerHeight;
    // 设置canvas高度为窗口内部高度

    canvas.style.backgroundColor = '#000';
    // 设置canvas的背景颜色为黑色

    document.body.style.overflow = 'hidden';
    // 设置页面body的溢出隐藏

    document.body.appendChild(canvas);
    // 将canvas元素添加到页面body中

    window.addEventListener('resize', function () {
      // 监听窗口大小调整事件

      const screenWidth = 1912;
      // 设置屏幕宽度为1912

      const screenHeight = 924;
      // 设置屏幕高度为924

      const zoomLevel = window.devicePixelRatio;
      // 获取设备像素比，并赋值给变量zoomLevel

      canvas = document.querySelector('canvas');
      // 获取canvas元素，并赋值给变量canvas

      canvas.style.width = window.innerWidth + 'px';
      // 设置canvas显示宽度为窗口内部宽度

      canvas.style.height = window.innerHeight + 'px';
      // 设置canvas显示高度为窗口内部高度

      canvas.width = window.innerWidth;
      // 设置canvas宽度为窗口内部宽度

      canvas.height = window.innerHeight;
      // 设置canvas高度为窗口内部高度

      canvas.style.backgroundColor = '#000';
      // 设置canvas的背景颜色为黑色

      deepCanvas.width = (screenWidth / zoomLevel.toFixed(2)).toFixed(2);
      // 计算deepCanvas宽度，并赋值给deepCanvas.width属性

      deepCanvas.height = (screenHeight / zoomLevel.toFixed(2)).toFixed(2);
      // 计算deepCanvas高度，并赋值给deepCanvas.height属性

      deepCanvas.style.backgroundColor = '#000';
      // 设置deepCanvas的背景颜色为黑色

      deepCanvas.style.width = (screenWidth / zoomLevel.toFixed(2)).toFixed(2) + 'px';
      // 计算deepCanvas显示宽度，并赋值给deepCanvas.style.width属性

      deepCanvas.style.height = (screenHeight / zoomLevel.toFixed(2)).toFixed(2) + 'px';
      // 计算deepCanvas显示高度，并赋值给deepCanvas.style.height属性
    })
    return deepCanvas;
    // 返回deepCanvas元素
  }
  function reactiveRectangle(rect, ctx) {
    // 定义函数reactiveRectangle

    let color = ['#ff0000', '#00ff00', '#0000ff', '#ffff00', '#00ffff', '#ff00ff'];
    // 定义颜色数组

    for (let i = 0, rectangles = rect.getRectangles(); i < rect.getRectangles().length; i++) {
      // 遍历矩形数组

      ctx.strokeStyle = color[rect.getIndexById(rectangles[i].id) % 6];
      // 根据矩形id索引获取对应颜色，并设置为绘制样式

      ctx.strokeRect(rectangles[i].space.max_x / 2 - rectangles[i].width / 2 + rectangles[i].space.x,
        rectangles[i].space.max_y / 2 - rectangles[i].height / 2 + rectangles[i].space.y, rectangles[i].width,
        rectangles[i].height);
      // 根据矩形空间坐标和尺寸绘制矩形
    }

    return ctx;
    // 返回绘制样式
  }
  function draw(rect) {
    try {
      // 尝试执行以下代码

      rect.updated();
      // 调用rect对象的updated方法

      let canvas = document.querySelector('canvas');
      // 获取canvas元素，并赋值给变量canvas

      let ctx = canvas.getContext('2d');
      // 获取canvas的2D绘图上下文，并赋值给变量ctx

      let deepCtx = deepCanvas.getContext('2d');
      // 获取deepCanvas的2D绘图上下文，并赋值给变量deepCtx

      ctx.clearRect(0, 0, canvas.width, canvas.height);
      // 清除canvas绘图区域

      deepCtx.clearRect(0, 0, deepCanvas.width, deepCanvas.height);
      // 清除deepCanvas绘图区域

      deepCtx.lineWidth = 4; // 设置deepCtx边框线宽

      deepCtx = reactiveRectangle(rect, deepCtx);
      // 调用reactiveRectangle函数，更新deepCtx样式

      ctx.drawImage(deepCanvas, rect.getSpace().x, rect.getSpace().y, canvas.width, canvas.height, 0, 0, canvas.width, canvas.height);
      // 在canvas上绘制deepCanvas图像

      requestAnimationFrame(function () {
        // 请求下一帧动画

        draw(rect);
        // 调用draw函数，绘制下一帧图形
      });
    }
    catch (e) {
      // 异常处理

      init();
      // 调用init函数，重新初始化
    }
  }
});