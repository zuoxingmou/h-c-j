let canvas: HTMLCanvasElement = document.querySelector('canvas') as HTMLCanvasElement
canvas.width = screen.availWidth
canvas.height = screen.availHeight

let ctx: CanvasRenderingContext2D = canvas.getContext('2d') as CanvasRenderingContext2D
let str: string[] = '01010asdas1010sads1011562212301'.split('')
let arr: Array<number> = new Array(Math.ceil(canvas.width / 10)).fill(0)
function render(): void {
  ctx.fillStyle = 'rgba(0, 0, 0,0.05)'
  ctx.fillRect(0, 0, canvas.width, canvas.height)
  ctx.fillStyle = '#0f0'
  arr.forEach((v, i) => {
    ctx.fillText(str[Math.floor(Math.random() * str.length)], i * 10, v + 10)
    arr[i] = arr[i] > canvas.height || arr[i] > Math.random() * 10000 ? 0 : arr[i] + 10
  })
}

setInterval(render, 1000 / 60)
