/*import { IndexedDB, IndexItem } from "./indexedDB.js"
    class Data {
      constructor(name, pw) {
        this.name = name;
        this.pw = pw;
      }
    }
    let DB = new IndexedDB({
      dbName: 'test',
      version: 1,
      objectStoreName: 'data',
      keyPath: { name: 'uid', isIncrement: true },
      index: [
        new IndexItem('uid', true),
        new IndexItem('name', false),
        new IndexItem('pw', false),
      ]
    })
    await DB.openDB();
    console.log(await DB.addData(new Data('zxw1', 525252525)));
    console.log(await DB.getDataByIndex('pw', 66666666666));
    console.log(await DB.getDataByIndexAll('name', 'zxw1'));
    console.log(await DB.getDataByIndexAllWithLimit('pw', 525252525, 10));
    console.log(await DB.getDataByIndexPage('pw', 1, 10));
*/

// 导出一个类：IndexItem
export class IndexItem {
  // 构造函数，接受三个参数：name，unique，arr
  constructor(name, unique, ...arr) {
    // 如果arr参数长度不为0，则抛出错误
    if (arr.length) {
      throw new Error('indexedItem 参数过多');
    }
    // 将name添加到DataList数组中
    DataList.push(name);
    // 将name赋值给this.name属性
    this.name = name;
    // 将name赋值给this.key属性
    this.key = name;
    // 将unique赋值给this.unique属性
    this.unique = unique;
  }
}
// IndexedDB类，用于操作IndexedDB数据库
export class IndexedDB {
  /**
   * IndexedDB构造函数
   * @param {object} options - 初始化参数对象
   * @param {string} options.dbName - 数据库名称
   * @param {number} options.version - 数据库版本号，默认为1
   * @param {string} options.objectStoreName - 对象存储名称
   * @param {object} options.keyPath - 唯一键路径，默认为{ name: 'id', isIncrement: true }
   * @param {array} options.index - 索引数组
   *   let DB = new indexedDB({
    dbName: 'test',
    version: 1,
    objectStoreName: 'data',
    keyPath: { name: 'uid', isIncrement: true },
    index: [
    {name,key,unique}
    ]
  })
   */
  constructor({ dbName, version = 1, objectStoreName,
    keyPath = { name: 'id', isIncrement: true }, index }) {
    this.dbName = dbName;
    this.version = version;
    this.db = null;
    this.objectStoreName = objectStoreName;
    this.index = index;
    this.keyPath = keyPath;
    this.DataList = DataList;
  }


  /**
   * 打开数据库
   * @returns {Promise} - 打开数据库的Promise对象
   */
  async openDB() {
    let self = this;
    return new Promise((resolve, reject) => {
      let indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
      if (!indexedDB) {
        reject(new Error("IndexedDB不被支持"));
        return;
      }
      let request = indexedDB.open(this.dbName, this.version);
      request.onsuccess = (event) => {
        this.db = event.target.result;
        resolve(event.target.result);
      };
      request.onerror = (event) => {
        reject(new Error("打开数据库时出错"));
      }
      request.onupgradeneeded = function (event) {
        let db = event.target.result;
        let objectStore = db.createObjectStore(self.objectStoreName, {
          keyPath: self.keyPath.name
          , autoIncrement: self.keyPath.isIncrement
        });
        if (self.index) {
          self.index.forEach((item) => {
            objectStore.createIndex(item.name, item.key, { unique: item.unique })
          })
        }
      }
    })
  }

  /**
   * 添加数据到数据库
   * @param {object} data - 要添加的数据
   * @returns {Promise} - 添加数据的Promise对象
   */
  async addData(data) {
    return new Promise((resolve, reject) => {
      if (!this.db) {
        throw new Error("数据库未打开");
      }
      let request = this.db.transaction([this.objectStoreName], "readwrite")
        .objectStore(this.objectStoreName).add(data);
      request.onsuccess = (event) => {
        resolve(true);
      }
      request.onerror = (event) => {
        reject(new Error("添加数据时出错"));
      }
    })
  }

  /**
   * 根据键获取数据
   * @param {string} key - 键值
   * @returns {Promise} - 获取数据的Promise对象
   */
  async getDataByKey(key) {
    return new Promise((resolve, reject) => {
      if (!this.db) {
        throw new Error("数据库未打开");
      }
      let request = this.db.transaction([this.objectStoreName], "readonly")
        .objectStore(this.objectStoreName).get(key);
      request.onsuccess = (event) => {
        resolve(request.result);
      }
      request.onerror = (event) => {
        reject(false);
      }
    })
  }

  /**
   * 根据索引获取数据
   * @param {string} indexName - 索引名称
   * @param {any} indexValue - 索引值
   * @returns {Promise} - 获取数据的Promise对象
   */
  async getDataByIndex(indexName, indexValue) {
    return new Promise((resolve, reject) => {
      if (!this.db) {
        throw new Error("数据库未打开");
      }
      let request = this.db.transaction([this.objectStoreName], "readonly")
        .objectStore(this.objectStoreName).index(indexName).get(indexValue);
      request.onsuccess = (event) => {
        resolve(request.result);
      }
      request.onerror = (event) => {
        reject(false);
      }
    })
  }

  /**
   * 获取所有符合索引条件的数据
   * @param {string} indexName - 索引名称
   * @param {any} indexValue - 索引值
   * @returns {Promise} - 获取数据的Promise对象
   */
  async getDataByIndexAll(indexName, indexValue) {
    return new Promise((resolve, reject) => {
      let list = [];
      if (!this.db) {
        throw new Error("数据库未打开");
      }
      let request = this.db.transaction([this.objectStoreName], "readonly")
        .objectStore(this.objectStoreName).index(indexName).openCursor(IDBKeyRange.only(indexValue));
      request.onsuccess = (event) => {
        let cursor = event.target.result;
        if (cursor) {
          list.push(cursor.value);
          cursor.continue();
        } else {
          resolve(list);
        }
      }
      request.onerror = (event) => {
        reject(false);
      }
    })
  }

  /**
   * 获取所有符合索引条件的数据（带有limit限制）
   * @param {string} indexName - 索引名称
   * @param {any} indexValue - 索引值
   * @param {number} limit - 数据限制数量
   * @returns {Promise} - 获取数据的Promise对象
   */
  async getDataByIndexAllWithLimit(indexName, indexValue, limit) {
    return new Promise((resolve, reject) => {
      let list = [];
      let listEnd = [];
      if (!this.db) {
        throw new Error("数据库未打开");
      }
      let request = this.db.transaction([this.objectStoreName], "readonly")
        .objectStore(this.objectStoreName).index(indexName).openCursor(IDBKeyRange.only(indexValue));
      request.onsuccess = (event) => {
        let cursor = event.target.result;
        if (cursor) {
          list.push(cursor.value);
          cursor.continue();
          if (list.length >= limit) {
            listEnd = JSON.parse(JSON.stringify(list));
            resolve(listEnd);
            return;
          }
        } else {
          listEnd = JSON.parse(JSON.stringify(list));
          resolve(listEnd);
        }
      }
      request.onerror = (event) => {
        reject(false);
      }
    })
  }

  /**
   * 根据索引分页获取数据
   * @param {string} indexName - 索引名称
   * @param {number} page - 页码
   * @param {number} limit - 数据限制数量
   * @returns {Promise} - 获取数据的Promise对象
   */
  async getDataByIndexPage(indexName, page, limit) {
    return new Promise((resolve, reject) => {
      let list = [];
      let listEnd = [];
      let advance = true
      if (!this.db) {
        throw new Error("数据库未打开");
      }
      let request = this.db.transaction([this.objectStoreName], "readonly")
        .objectStore(this.objectStoreName).index(indexName).openCursor();
      request.onsuccess = (event) => {
        let cursor = event.target.result;
        if (page > 1 && advance) {
          advance = false;
          cursor.advance((page - 1) * limit);
          return;
        }
        if (cursor) {
          list.push(cursor.value);
          if (list.length >= limit) {
            listEnd = JSON.parse(JSON.stringify(list));
            resolve(listEnd);
          }
          cursor.continue();
        } else {
          listEnd = JSON.parse(JSON.stringify(list));
          resolve(listEnd);
        }
      }
      request.onerror = (event) => {
        reject(false);
      }
    })
  }
}